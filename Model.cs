using SQLite4Unity3d;
using UnityEngine;

namespace Sheilta {
    public class Model  {

        [PrimaryKey, AutoIncrement]
		public int Id { get; set; }

        public Model Save() {
            if (Id == 0) {
				Id = DataService.Instance.Connection.Insert (this);
            } else {
				DataService.Instance.Connection.Update (this);
            }

			return this;
        }

        public void Delete() {
			if (Id != 0) {
				DataService.Instance.Connection.Delete (this);
			}
        }

		public Model(){
		}

		public virtual void CreateTable (SQLiteConnection connection) {}
    }
}