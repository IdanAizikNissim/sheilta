using System;
using System.Reflection;
using System.Linq;
using SQLite4Unity3d;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if !UNITY_EDITOR
using System.IO;
#endif

namespace Sheilta {
	public class DataService  {
		private static DataService _instance;
		private SQLiteConnection _connection;

		public SQLiteConnection Connection {
			get {
				return _connection;
			}
		}

		public static void Connect(string DatabaseName) {
			if (_instance == null) {
				_instance = new DataService (DatabaseName + ".db");
			}
		}

		public static DataService Instance {
			get {
				return _instance;
			}
		}

		private DataService(string DatabaseName) {
			#if UNITY_EDITOR
				var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
			#else
				var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);
				if (!File.Exists(filepath)) {
					var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;
					File.Copy(loadDb, filepath);
				}
				
				var dbPath = filepath;
			#endif

			_connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
		}

		public void CreateTable(Type type) {
			((Model)Activator.CreateInstance (type)).CreateTable (_connection);
		}

		public bool ExistsTable(Type type) {
			return _connection.ExistsTable (type.Name);
		}
	}
}