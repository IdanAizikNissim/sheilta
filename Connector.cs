﻿using UnityEngine;

namespace Sheilta
{
    public class Connector : MonoBehaviour
    {
        public string DB;

        void Start()
        {
            DataService.Connect(DB);
        }
    }
}